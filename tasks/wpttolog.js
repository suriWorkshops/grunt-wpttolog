var webpagetest = require('webpagetest');
var format = require('string-format');
var async = require('async');
var fs = require('fs');
var YSlow = require('yslow');
var JsDom = require('jsdom');
var wpt = null;

module.exports = function(grunt) {
    var checkTestStatus = function(wpt, testId, options, done){
        wpt.getTestStatus(testId, function(err, data) {

            if (err) {
                 grunt.log.ok("Error: " + err);
                return done(err);
            }

            grunt.log.ok("Status for " + testId + ": " + data.data.statusText);

            if (!data.data.completeTime) {
                setTimeout(function(){
                    checkTestStatus(wpt, testId, options, done);
                }, 50000);
            }
            else {
                return wpt.getTestResults(testId, function(err, data) {
                    grunt.log.ok(options.instanceUrl + "/result/" + testId + "/");

                    if (err > 0) {
                        return done(err);
                    }

                    var message = format('WPT results: <a href="{0}">{0}</a><br />Page under test: {1}<br /> Load Time: {2} <br />TTFB: {3}',data.data.summary, options.testUrl, data.data.median.firstView.loadTime, data.data.median.firstView.TTFB);
                    grunt.log.ok(message);

                    delete data.data.runs;

                    async.series([
                       function(callback) {
                            if (options.notifyLogstash) {
                                grunt.log.ok("Notify Logstash");
                                execute(this, data, options, testId, callback);
                            }
                            else {
                                callback();
                            }
                        }
                    ],
                    function(err, data) {
                        done();
                    });
                });
            }
        });
    };

    var execute = function(task, data, options,testId, done){

        data.data.median.firstView.requests = [];
        data.data.median.repeatView.requests = [];
        data.data.median.firstView.domains = [];
        data.data.median.repeatView.domains = [];

        //MEDIAN
        var list = [];
        for (var key in data.data.median.firstView) {
            if (key.indexOf("CSI") != -1) {
                list.push(key);
            }
        }
        for (i = 0; i < list.length; i++) {
            delete data.data.median.firstView[list[i]];
        }
        var list = [];
        for (var key in data.data.median.repeatView) {
            if (key.indexOf("CSI") != -1) {
                list.push(key);
            }
        }
        for (i = 0; i < list.length; i++) {
            delete data.data.median.repeatView[list[i]];
        }

        //AVERAGE
        var list = [];
        for (var key in data.data.average.firstView) {
            if (key.indexOf("CSI") != -1) {
                list.push(key);
            }
        }
        for (i = 0; i < list.length; i++) {
            delete data.data.average.firstView[list[i]];
        }
        var list = [];
        for (var key in data.data.average.repeatView) {
            if (key.indexOf("CSI") != -1) {
                list.push(key);
            }
        }
        for (i = 0; i < list.length; i++) {
            delete data.data.average.repeatView[list[i]];
        }

        //standardDeviation
        var list = [];
        for (var key in data.data.standardDeviation.firstView) {
            if (key.indexOf("CSI") != -1) {
                list.push(key);
            }
        }
        for (i = 0; i < list.length; i++) {
            delete data.data.standardDeviation.firstView[list[i]];
        }
        var list = [];
        for (var key in data.data.standardDeviation.repeatView) {
            if (key.indexOf("CSI") != -1) {
                list.push(key);
            }
        }
        for (i = 0; i < list.length; i++) {
            delete data.data.standardDeviation.repeatView[list[i]];
        }
        
        if (options.performancecriteria != null) {
            var performancestatus = checkCriteriaLevel(data, options);
            data.data.performancecriteria = options.performancecriteria;
            data.data.performanceStatus = performancestatus;
            data.data.performancePoints = checkPerformancePoints(performancestatus);
        }

        grunt.log.ok("PERFORMANCE CRITERIA: " + data.data.performanceStatus);

        wpt.getHARData(testId, function (err, harData) {
            var har = harData,
            YSLOW = YSlow.YSLOW,
            doc = JsDom.jsdom(),
            res = YSLOW.harImporter.run(doc, har, 'ydefault'),
            content = YSLOW.util.getResults(res.context, 'basic');
            data.data.yslow = content.o;

            var wptData = JSON.stringify(data);

            var output = 'log/wpt' + grunt.template.today('yyyy-mm-dd-HH-MM') + '.log';
            console.log('Writing file: ' + output);
            fs.appendFile(output, wptData + '\n');
          });
    };

    var checkPerformancePoints = function(performancestatus){
        var points = 0;
        if (performancestatus == "D"){
            points = 1; 
        }
        if (performancestatus == "C"){
            points = 50;
        }
        if (performancestatus == "B"){
            points = 75;
        }
        if (performancestatus == "A"){
            points = 100;
        }
        return points;
    };

    var checkCriteriaLevel = function(data, options){

        var factorDeviation = (options.performancecriteria.deviationPercentage / 100) + 1;
        var level ="D";

        console.log('DATA WPT FIRST VIEW');
        console.log('====================');
        console.log('load time: ' + data.data.median.firstView.loadTime);
        console.log('TTFB: ' + data.data.median.firstView.TTFB);
        console.log('bytes: ' + data.data.median.firstView.bytesInDoc/1024);
        console.log('Speed Index: ' + data.data.median.firstView.SpeedIndex);
        console.log('requests: ' + data.data.median.firstView.requestsDoc);
        console.log('render: ' + data.data.median.firstView.render);

        console.log('DATA WPT REPEAT VIEW');
        console.log('=====================');
        console.log('load time: ' + data.data.median.repeatView.loadTime);
        console.log('TTFB: ' + data.data.median.repeatView.TTFB);
        console.log('bytes: ' + data.data.median.repeatView.bytesInDoc/1024);
        console.log('Speed Index: ' + data.data.median.repeatView.SpeedIndex);
        console.log('requests: ' + data.data.median.repeatView.requestsDoc);
        console.log('render: ' + data.data.median.repeatView.render);
       

        if (data.data.median.firstView.loadTime <= (options.performancecriteria.C.firstView.loadTime * factorDeviation) &&
            data.data.median.firstView.TTFB <= (options.performancecriteria.C.firstView.TTFB * factorDeviation) &&
            (data.data.median.firstView.bytesInDoc / 1024) <= (options.performancecriteria.C.firstView.bytesInDoc * factorDeviation) &&
            //data.data.median.firstView.domElements <=  options.performancecriteria.C.firstView.domElements &&
            data.data.median.firstView.SpeedIndex <= (options.performancecriteria.C.firstView.SpeedIndex * factorDeviation) &&
            data.data.median.firstView.requestsDoc <=  (options.performancecriteria.C.firstView.requestsDoc * factorDeviation) &&
            data.data.median.firstView.render <= (options.performancecriteria.C.firstView.render * factorDeviation) &&
            data.data.median.repeatView.loadTime <= (options.performancecriteria.C.repeatView.loadTime * factorDeviation) &&
            data.data.median.repeatView.TTFB <= (options.performancecriteria.C.repeatView.TTFB * factorDeviation) &&
            (data.data.median.repeatView.bytesInDoc / 1024) <= (options.performancecriteria.C.repeatView.bytesInDoc * factorDeviation) &&
            //data.data.median.repeatView.domElements <=  options.performancecriteria.C.repeatView.domElements &&
            data.data.median.repeatView.SpeedIndex <= (options.performancecriteria.C.repeatView.SpeedIndex * factorDeviation) &&
            data.data.median.repeatView.requestsDoc <=  (options.performancecriteria.C.repeatView.requestsDoc * factorDeviation) &&
            data.data.median.repeatView.render <= (options.performancecriteria.C.repeatView.render * factorDeviation)){
                level = "C";
        }
        if (data.data.median.firstView.loadTime <= (options.performancecriteria.B.firstView.loadTime * factorDeviation) &&
            data.data.median.firstView.TTFB <= (options.performancecriteria.B.firstView.TTFB * factorDeviation) &&
            (data.data.median.firstView.bytesInDoc / 1024) <= (options.performancecriteria.B.firstView.bytesInDoc * factorDeviation) &&
            //data.data.median.firstView.domElements <=  options.performancecriteria.B.firstView.domElements &&
            data.data.median.firstView.SpeedIndex <= (options.performancecriteria.B.firstView.SpeedIndex * factorDeviation) &&
            data.data.median.firstView.requestsDoc <=  (options.performancecriteria.B.firstView.requestsDoc * factorDeviation) &&
            data.data.median.firstView.render <= (options.performancecriteria.B.firstView.render * factorDeviation) &&
            data.data.median.repeatView.loadTime <= (options.performancecriteria.B.repeatView.loadTime * factorDeviation) &&
            data.data.median.repeatView.TTFB <= (options.performancecriteria.B.repeatView.TTFB * factorDeviation) &&
            (data.data.median.repeatView.bytesInDoc / 1024) <= (options.performancecriteria.B.repeatView.bytesInDoc * factorDeviation) &&
            //data.data.median.repeatView.domElements <=  options.performancecriteria.B.repeatView.domElements &&
            data.data.median.repeatView.SpeedIndex <= (options.performancecriteria.B.repeatView.SpeedIndex * factorDeviation) &&
            data.data.median.repeatView.requestsDoc <=  (options.performancecriteria.B.repeatView.requestsDoc * factorDeviation) &&
            data.data.median.repeatView.render <= (options.performancecriteria.B.repeatView.render * factorDeviation)){
                level = "B";
        }
        if (data.data.median.firstView.loadTime <= (options.performancecriteria.A.firstView.loadTime * factorDeviation) &&
            data.data.median.firstView.TTFB <= (options.performancecriteria.A.firstView.TTFB * factorDeviation) &&
            (data.data.median.firstView.bytesInDoc / 1024) <= (options.performancecriteria.A.firstView.bytesInDoc * factorDeviation) &&
            //data.data.median.firstView.domElements <=  options.performancecriteria.A.firstView.domElements &&
            data.data.median.firstView.SpeedIndex <= (options.performancecriteria.A.firstView.SpeedIndex * factorDeviation) &&
            data.data.median.firstView.requestsDoc <=  (options.performancecriteria.A.firstView.requestsDoc * factorDeviation) &&
            data.data.median.firstView.render <= (options.performancecriteria.A.firstView.render * factorDeviation) &&
            data.data.median.repeatView.loadTime <= (options.performancecriteria.A.repeatView.loadTime * factorDeviation) &&
            data.data.median.repeatView.TTFB <= (options.performancecriteria.A.repeatView.TTFB * factorDeviation) &&
            (data.data.median.repeatView.bytesInDoc / 1024) <= (options.performancecriteria.A.repeatView.bytesInDoc * factorDeviation) &&
            //data.data.median.repeatView.domElements <=  options.performancecriteria.A.repeatView.domElements &&
            data.data.median.repeatView.SpeedIndex <= (options.performancecriteria.A.repeatView.SpeedIndex * factorDeviation) &&
            data.data.median.repeatView.requestsDoc <=  (options.performancecriteria.A.repeatView.requestsDoc * factorDeviation) &&
            data.data.median.repeatView.render <= (options.performancecriteria.A.repeatView.render * factorDeviation)){
                level = "A";
        }
   
        return level;
    };


    var makeRequest = function(task, done) {
        var options = task.options({
            instanceUrl: 'www.webpagetest.org',
            wptApiKey: null,
            testUrl: 'http://www.google.com',
            runs: 5,
            logstashHost: 'localhost',
            logstashPort: null,
            notifyLogstash: true,
            location: 'Test:Chrome',
            label: 'WPT',
            private: false,
            connectivity: 'Cable',
            video: false,
            performancecriteria: null
        });

        var parameters = {
            runs: options.runs,
            location: options.location,
            private: options.private,
            label: options.label,
            connectivity: options.connectivity,
            video: options.video
        };

        grunt.log.ok("OPTIONS: " + JSON.stringify(options));

        wpt = new webpagetest(options.instanceUrl, options.wptApiKey);
        var testId;
        wpt.runTest(options.testUrl, parameters, function(err, data) {
           if (data.statusCode === 200) {
               grunt.log.ok('WPT launched OK');
               testId = data.data.testId;
               checkTestStatus(wpt, testId, options, done);
           }
        });
    };

    grunt.registerMultiTask('wpttolog', function() {
        var done = this.async();
        makeRequest(this, done);
    });
};
